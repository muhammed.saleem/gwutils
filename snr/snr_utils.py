
import sys
import numpy as np
import gwutils.snr.pop_utils as pput
import lal
import lalsimulation as lalsim
#import pycbc.detector as pydet
import pycbc.waveform
# For the updated location/orientation of Aundha, use Aditya's bilby branch 
import bilby

'''
DETS = {}
DETS['H1'] = pydet.Detector(detector_name='H1', reference_time=1126259462.0)
DETS['L1'] = pydet.Detector(detector_name='L1', reference_time=1126259462.0)
DETS['V1'] = pydet.Detector(detector_name='V1', reference_time=1126259462.0)
DETS['K1'] = pydet.Detector(detector_name='K1', reference_time=1126259462.0)
DETS['I1'] = pydet.Detector(detector_name='I1', reference_time=1126259462.0)
DETS = {}
DETS['H1'] = pydet.Detector(detector_name='H1')
DETS['L1'] = pydet.Detector(detector_name='L1')
DETS['V1'] = pydet.Detector(detector_name='V1')
DETS['K1'] = pydet.Detector(detector_name='K1')
DETS['I1'] = pydet.Detector(detector_name='I1')
'''

def gpst_2_gmst(gps):
    k=13713.440712226984
    c=45991.090394612074
    gmst = gps/k -c
    return 'GMST = ',gmst,' GMST in radian = ', np.mod(gmst,2*np.pi)

def gpst_2_gmstRad(gps):
    k=13713.440712226984
    c=45991.090394612074
    gmst = gps/k -c
    return np.mod(gmst,2*np.pi)
    

def raDec2PhiTheta(ra,dec,gpst):
	gmstRad = gpst_2_gmstRad(gpst)
	theta=np.pi/2-dec
	phi=ra-gmstRad
	return phi, theta

def PhiTheta2raDec(phi,theta,gpst):
	gmstRad = gpst_2_gmstRad(gpst)
	dec = np.pi/2-theta
	ra  = gmstRad+phi
	if ra> (2*np.pi):
		ra = ra - (2*np.pi)
	return ra,dec



'''
def fplus_fcross_pycbc (detector = 'H1', theta=0.0, phi=0.0, psi=0.0):
    det = DETS[detector]
    reference_time=1126259462.0
    ra, dec = PhiTheta2raDec(phi=phi,theta=theta,gpst=reference_time)
    fp,fc = det.antenna_pattern(right_ascension=ra, declination=dec,polarization=psi,t_gps = reference_time)
    return fp, fc
'''

def fplus_fcross (detector = 'H1', dec=0.0, ra=0.0, psi=0.0, gpst=1126259462.0):
    fp = bilby.gw.detector.get_empty_interferometer(detector).antenna_response(ra, dec, gpst, psi, "plus")
    fc = bilby.gw.detector.get_empty_interferometer(detector).antenna_response(ra, dec, gpst, psi, "cross")
    return fp, fc


def fvec_4_hp (hp):
    N = len(hp.data.data)
    fmax = (N-1) * hp.deltaF
    f = np.arange( hp.f0, fmax+hp.deltaF, hp.deltaF)
    return f


######### waveform call
# currently include only dquadmon1 and 2 inputs
# can be generelized for any other params like lambda1 and 2 etc.
#


def innerProduct ( df, h1, h2 ):
    return (4.0*np.sum(df*h1*np.conj(h2)));

def optimal_snr ( df, f_ref = 0,
                            f_min	= 10.0,
                            f_max	= 2048.0,
                            deltaF	= 0.005,
                            detector= ['L1','H1','A1'],
                            psdfn	= [pput.aligo,pput.aligo,pput.aligo],
                            wfmodel     = 'IMRPhenomPv2'):

        '''
        	Computes SNR at any detector structures and sensiivity (by default for L1 with aligo)
        '''
        
        m1det	= df['mass_1']
        m2det	= df['mass_2']
        DL		= df['luminosity_distance']
        dec	    = df['dec']
        ra		= df['ra']
        gpst    = df['geocent_time']
        psi		= df['psi']
        iota	= df['theta_jn']
        coa_phase = df['phase']
        S1x = df['a_1'] * np.sin(df['tilt_1']) * np.cos(df['phi_jl'])
        S1y = df['a_1'] * np.sin(df['tilt_1']) * np.sin(df['phi_jl'])
        S1z = df['a_1'] * np.cos(df['tilt_1']) 
        S2x = df['a_2'] * np.sin(df['tilt_2']) * np.cos(df['phi_12'])
        S2y = df['a_2'] * np.sin(df['tilt_2']) * np.sin(df['phi_12'])
        S2z = df['a_2'] * np.cos(df['tilt_2']) 
        phiRef = 0
        eccentricity = 0.0
        meanPerAno   = 0.0
        longAscNodes = 0.0
        Lambda1      = 0.0
        Lambda2      = 0.0
        dkappa1  = 0.0
        dkappa2  = 0.0        

        hp,hc = pycbc.waveform.get_fd_waveform(mass1 = m1det, 
				                mass2  = m2det,
                                spin1x =  S1x,
                                spin1y =  S1y,
                                spin1z =  S1z,
                                spin2x =  S2x,
                                spin2y =  S2y,
                                spin2z =  S2z,
				                distance	= DL,
                                coa_phase = coa_phase,
                                inclination = iota,
                                dquad_mon1 = dkappa1,
                                dquad_mon2 = dkappa2,
				                delta_f	= deltaF, 
				                f_lower	= f_min,
                                f_final = f_max,
				                approximant= wfmodel, 
				 )
		
        f 		= np.array(hp.sample_frequencies.data)
        inBand  = (f>=f_min)&(f<=f_max)
        f       = f[inBand]   # trimmed the zero paddings
        hpf		= np.array(hp.data)[inBand]
        hcf		= np.array(hc.data)[inBand]
                #------------------------------------
        rho = []
        for DET,PSD in zip(list(detector),list(psdfn)):
            Snf		= PSD(f)
            Fp, Fc  = fplus_fcross (DET, dec, ra, psi, gpst)
            hf  	= hpf * Fp  + hcf * Fc
            #print len(hf), len(Snf)
            rho += [np.sqrt(np.real(innerProduct( deltaF, hf, hf/Snf)))]
        return rho


def get_lalsim_wf (m1 = 10.0, m2 = 5.0,  # both in Msun
                   distance = 1000.0,   # in Mpc
                   inclination=0.0,
                   #
                   f_min =  20.0,f_max = 2048.0,deltaF = 0.005,
                   #
                   S1x = 0.0,S1y = 0.0,S1z = 0.0,
                   S2x = 0.0,S2y = 0.0,S2z = 0.0,
                   #
                   phiRef = 0,
                   f_ref  = 0,
                   eccentricity = 0.0,
                   meanPerAno   = 0.0,
                   longAscNodes = 0.0,
                   #
                   Lambda1  = 0.0,
                   Lambda2  = 0.0,
                   dkappa1  = 0.0,
                   dkappa2  = 0.0,
                   model    = lalsim.IMRPhenomPv2,
                   returnObject = False  # returns the hp,hc objects instead of data
                   ):

    # process starts
    [m1SI, m2SI, distanceSI] = [m1*lal.MSUN_SI, m2*lal.MSUN_SI, distance*lal.PC_SI*1e6]
    # LAL Dictionary with BH kappa (specify explicitly dQuadMon1 and 2)
    print ('adding dk1,dk2 = ',dkappa1,dkappa2)
    params = lal.CreateDict()
    lalsim.SimInspiralWaveformParamsInsertTidalLambda1(params, Lambda1)
    lalsim.SimInspiralWaveformParamsInsertTidalLambda2(params, Lambda1)
    lalsim.SimInspiralWaveformParamsInsertdQuadMon1(params, dkappa1)
    lalsim.SimInspiralWaveformParamsInsertdQuadMon2(params, dkappa2)
    m = m1 + m2
    msols = m*lal.MTSUN_SI
    # Generating waveform
    hp, hc = lalsim.SimInspiralChooseFDWaveform(
	m1SI,                           # mass of companion 1 (kg)
	m2SI,                           # mass of companion 2 (kg)
	S1x,S1y,S1z,S2x,S2y,S2z,        # all the components of the dimensionless spins 1 and 2
	distanceSI,                     # distance of source (m)
	inclination,                    # inclination of source (rad)
	phiRef,                         # reference orbital phase (rad)
	longAscNodes,                   # longitude of ascending nodes, degenerate with the polarization angle, Omega in documentation
	eccentricity,                   # eccentricity at reference epoch
	meanPerAno,                     # mean anomaly of periastron
	deltaF,                         # sampling interval (Hz)
	f_min,                          # starting GW frequency (Hz)
	f_max,                          # final GW frequency (Hz)
	f_ref,                          # reference GW frequency (Hz)
	params,                 	# LAL dictionary containing accessory parameters
	model                           # post-Newtonian approximant to use for waveform production
	)

    hpf = hp.data.data
    hcf = hc.data.data
    f_lal = fvec_4_hp (hp)
    low_index = int(f_min / deltaF) 
    f_lal = f_lal[low_index:] 
    hpf = hpf[low_index:] 
    hcf = hcf[low_index:] 
    if returnObject == False:
        return f_lal, hpf, hcf
    else:
        return f_lal, hp, hc
#################################


######################
	    
def LonLat2raDec(Lon,Lat,gpst):  
    # not approved, only a trial
    # Use at own risk!
	PI=np.pi;PIbyTWO = 0.5*PI
	gmstRad = gpst_2_gmstRad(gpst)
	if Lat > PIbyTWO:
		dec = PIbyTWO - Lat
	else:
		dec = Lat

	if Lon > PI:
		Lon = Lon - PI
	if Lon < -PI:
		Lon = Lon + PI

	if Lon > 0:
		ra = gmstRad+ Lon
	if Lon < 0:
		ra = gmstRad+ (2*PI-abs(Lon))
	if ra> (2*PI):
		ra = ra - (2*PI)
	return ra,dec

def iota_factors (iota):
    plus = 0.5 * (1+np.cos(iota)**2)
    cross = np.cos(iota)
    return plus, cross
    

def optimal_snr_faster ( df, 
                            m1det = 10,
                            m2det = 10,
                            DL = 100,
                            f_ref = 0,
                            f_min	= 10.0,
                            f_max	= 2048.0,
                            deltaF	= 0.005,
                            detector= ['L1','H1','A1'],
                            psdfn	= [pput.aligo,pput.aligo,pput.aligo],
                            wfmodel     = 'IMRPhenomPv2'):

        '''
        	Computes SNR at any detector structures and sensiivity (by default for L1 with aligo)
        '''
        
        m1det	= df['mass_1']
        m2det	= df['mass_2']
        DL		= df['luminosity_distance']
        dec	    = df['dec']
        ra		= df['ra']
        gpst    = df['geocent_time']
        psi		= df['psi']
        iota	= df['theta_jn']
        coa_phase = df['phase']
        S1x = df['a_1'] * np.sin(df['tilt_1']) * np.cos(df['phi_jl'])
        S1y = df['a_1'] * np.sin(df['tilt_1']) * np.sin(df['phi_jl'])
        S1z = df['a_1'] * np.cos(df['tilt_1']) 
        S2x = df['a_2'] * np.sin(df['tilt_2']) * np.cos(df['phi_12'])
        S2y = df['a_2'] * np.sin(df['tilt_2']) * np.sin(df['phi_12'])
        S2z = df['a_2'] * np.cos(df['tilt_2']) 
        phiRef = 0
        eccentricity = 0.0
        meanPerAno   = 0.0
        longAscNodes = 0.0
        Lambda1      = 0.0
        Lambda2      = 0.0
        dkappa1  = 0.0
        dkappa2  = 0.0        

        
        
        ## iota dependence is a prefactor. We compute waveform frequency series for a fixed iota = 1 and then scale it for any given iota
        hp_iota_1,hc_iota_1 = pycbc.waveform.get_fd_waveform(mass1 = m1det, 
				                mass2  = m2det,
                                spin1x =  S1x,
                                spin1y =  S1y,
                                spin1z =  S1z,
                                spin2x =  S2x,
                                spin2y =  S2y,
                                spin2z =  S2z,
                                coa_phase = coa_phase,
				                distance	= DL,
                                inclination = 1,
                                dquad_mon1 = dkappa1,
                                dquad_mon2 = dkappa2,
				                delta_f	= deltaF, 
				                f_lower	= f_min,
                                f_final = f_max,
				                approximant= wfmodel, 
				 )
        
        ifac_fixed = iota_factors (iota = 1)
        ifac_actual = iota_factors (iota = iota)
        iota_scaling_p = ifac_actual[0]/ifac_fixed[0]
        iota_scaling_c = ifac_actual[1]/ifac_fixed[1]
        
        f 		= np.array(hp_iota_1.sample_frequencies.data)
        inBand  = (f>=f_min)&(f<=f_max)
        f       = f[inBand]   # trimmed the zero paddings
        hpf		= np.array(hp_iota_1.data)[inBand]
        hcf		= np.array(hc_iota_1.data)[inBand]
        
        #------------------------------------
        inner_hphp = []
        inner_hchc = []
        inner_hphc = []
        inner_hchp = []
        
        for PSD in list(psdfn):
            Snf		= PSD(f)
            inner_hphp += [np.real(innerProduct( deltaF, hpf, hpf/Snf))]
            inner_hchc += [np.real(innerProduct( deltaF, hcf, hcf/Snf))]
            inner_hphc += [np.real(innerProduct( deltaF, hpf, hcf/Snf))]
            inner_hchp += [np.real(innerProduct( deltaF, hcf, hpf/Snf))]

        rho = []
        for i, DET in enumerate(list(detector)):
            Fp, Fc  = fplus_fcross (DET, dec, ra, psi, gpst)
            Fp *= iota_scaling_p
            Fc *= iota_scaling_c
            inner_hh = Fp**2 * inner_hphp[i] + Fc**2 * inner_hchc[i] + Fp*Fc * (inner_hphc[i] + inner_hchp[i])
            #print len(hf), len(Snf)
            rho += [np.sqrt( inner_hh )]
        return rho


def optimal_snr_test ( df, 
                            m1det = 10,
                            m2det = 10,
                            DL = 100,
                            a1 = 0,
                            a2 = 0,
                            f_ref = 0,
                            f_min	= 10.0,
                            f_max	= 2048.0,
                            deltaF	= 0.005,
                            detector= ['L1','H1','A1'],
                            psdfn	= [pput.aligo,pput.aligo,pput.aligo],
                            wfmodel     = 'IMRPhenomPv2'):

    '''
        Computes SNR at any detector structures and sensiivity (by default for L1 with aligo)
    '''
        
    dec	    = df['dec'].to_numpy()
    ra		= df['ra'].to_numpy()
    psi		= df['psi'].to_numpy()
    iota	= df['theta_jn'].to_numpy()
    gpst    = df['geocent_time'].to_numpy()
    phiRef = 0
    eccentricity = 0.0
    meanPerAno   = 0.0
    longAscNodes = 0.0
    Lambda1      = 0.0
    Lambda2      = 0.0
    dkappa1  = 0.0
    dkappa2  = 0.0        

    df['mass_1'] = m1det
    df['mass_2'] = m2det
    df['a1'] = a1
    df['a2'] = a2
    df['luminosity_distance'] = DL


    ## iota dependence is a prefactor. We compute waveform frequency series for a fixed iota = 1 and then scale it for any given iota
    hp_iota_1,hc_iota_1 = pycbc.waveform.get_fd_waveform(mass1 = m1det, 
                            mass2  = m2det,
                            spin1x =  0,
                            spin1y =  0,
                            spin1z =  a1,
                            spin2x =  0,
                            spin2y =  0,
                            spin2z =  a2,
                            distance	= DL,
                            inclination = 1,
                            dquad_mon1 = dkappa1,
                            dquad_mon2 = dkappa2,
                            delta_f	= deltaF, 
                            f_lower	= f_min,
                            f_final = f_max,
                            approximant= wfmodel 
             )


    ifac_fixed     = iota_factors (iota = 1)
    ifac_actual    = iota_factors (iota = iota)
    iota_scaling_p = ifac_actual[0]/ifac_fixed[0]
    iota_scaling_c = ifac_actual[1]/ifac_fixed[1]

    f 		= np.array(hp_iota_1.sample_frequencies.data)
    inBand  = (f>=f_min)&(f<=f_max)
    f       = f[inBand]   # trimmed the zero paddings
    hpf		= np.array(hp_iota_1.data)[inBand]
    hcf		= np.array(hc_iota_1.data)[inBand]

    #------------------------------------
    inner_hphp = []
    inner_hchc = []
    inner_hphc = []
    inner_hchp = []

    for PSD in list(psdfn):
        Snf		= PSD(f)
        inner_hphp += [np.real(innerProduct( deltaF, hpf, hpf/Snf))]
        inner_hchc += [np.real(innerProduct( deltaF, hcf, hcf/Snf))]
        inner_hphc += [np.real(innerProduct( deltaF, hpf, hcf/Snf))]
        inner_hchp += [np.real(innerProduct( deltaF, hcf, hpf/Snf))]

    rho = []
    for i, DET in enumerate(list(detector)):
        snr_key = f"snr {detector}"
        det = Detector(DET)
        Fp, Fc  =  det.antenna_pattern(right_ascension=ra, declination=dec,polarization=psi,t_gps = gpst)
        Fp *= iota_scaling_p
        Fc *= iota_scaling_c
        inner_hh = Fp**2 * inner_hphp[i] + Fc**2 * inner_hchc[i] + Fp*Fc * (inner_hphc[i] + inner_hchp[i])
        #print len(hf), len(Snf)
        df[snr_key] = np.sqrt( inner_hh )
        rho += [np.sqrt( inner_hh )]

    rho = np.array(rho)
    rho_net = np.sqrt(np.sum(rho**2, axis = 0))
    df['snr net'] = rho_net

    return rho, rho_net, df  



