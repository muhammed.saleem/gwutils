import numpy as np
import scipy 
import sys
import scipy.interpolate as si 
from scipy.integrate import quad
from scipy.integrate import simps

''' Conversion factors '''

''' Standard LCDM cosmology'''
OmegaM = 0.3
OmegaK = 0.0 
OmegaL = 0.7   

c=3.0e5;  # km/sec
h = 0.7
H0=100 * h; # km  sec^-1 Mpc^-1  
DH = c/H0; # Mpc    (Hubble distance)

def fn_E(z):
	return np.sqrt ( OmegaM * (1+z)**3  + OmegaL)


def Dc(z):  # in Mpc (since DH is in Mpc)
	
	def integrand (zprime):
		return DH / fn_E(zprime)
				
	return quad( integrand, 0, z, )[0]


def DL(z):
	return (1+z)*Dc(z)

def z_vs_D_interp ():
	z=np.linspace(0.0,10,20000)
	dc = z*0
	for i in range(len(dc)):
		dc[i] = Dc(z[i])
	dL = dc*(1+z)	
	dc_fof_z = si.interp1d(z, dc, kind='slinear')
	z_fof_dc = si.interp1d(dc, z, kind='slinear')
	dL_fof_z = si.interp1d(z, dL, kind='slinear')
	z_fof_dL = si.interp1d(dL, z, kind='slinear')
	return dc_fof_z, z_fof_dc, dL_fof_z, z_fof_dL

#############################################################
# 	making interpolated functions so that it can be quickly 
#	accessed without interpolating everytime.

# 	these functions can be used for conversions between Dc vs z or DL vs z and vice versa

# 	each of the four terms in LHS are functions 

# 	Eg, dc_fof_z (5) will return the Dc corresponding to z=5

dc_fof_z, z_fof_dc,dL_fof_z, z_fof_dL = z_vs_D_interp ()

	
def dVbydz (z): # comoving differential volume for flat (using interpolated Dc)
	Dcomoving = dc_fof_z(z)
	Ez = fn_E(z)
	return 4*np.pi* Dcomoving**2 * DH / Ez   # in Mpc^3

def dVbydz_GpcCube (z): # 
	return 1e-9 * dVbydz (z)  # in Gpc^3


def dNz_dz (Rz, z):  # Number of sources with redshift between z and z+dz, Rz should be rate density function 
	return Rz(z) * dVbydz_GpcCube (z) /(1+z) 
	
def dNz_dz_nonevolving (z=1):  # Number of sources with redshift between z and z+dz,
        constantRate=1
        return constantRate * dVbydz_GpcCube (z) /(1+z) 

def dNz_dz_gwtc2 (z, R0=23.071782, kappa=0.841695):  # no.of sources with redshift b/w z and z+dz
    return R0 * (1+z)**(kappa-1) * dVbydz_GpcCube (z)


	
def gen_random (xmin,xmax,Px,N=None):
	x  = np.linspace(xmin,xmax,300)
	px = np.zeros_like(x)
	dx = x[2] - x[1]
	dPx = Px(x)
	px[0] = dPx[0] * dx
	for i in range(len(x)-1):
		px[i+1] = px[i] + dPx[i+1] *dx

	x2px  = si.interp1d(x,px , kind='slinear')
	px2x  = si.interp1d(px,x , kind='slinear')
	px_min = min(px)
	px_max = max(px)
	if N==None:
		N = int(px_max)  # N is chosen as predicted by the particular model  ie, N = integral N(z) dz
	pxRandom = np.random.uniform(px_min,px_max,N)
	xrandom = px2x(pxRandom)
	return xrandom


# same as gen_random, a bit more generic but slower
def random_sampling (xmin,xmax,Px,N=None):
    x = np.sort(np.random.uniform(xmin,xmax,300))
    px = np.zeros_like(x)
    dx = x[2] - x[1]
    dPx = np.zeros_like(x)
    for i in range(len(x)):
        dPx[i] = Px(x[i])
    px[0] = dPx[0] * dx
    for i in range(len(x)-1):
        px[i+1] = px[i] + dPx[i+1] *dx

    x2px  = si.interp1d(x,px , kind='slinear')
    px2x  = si.interp1d(px,x , kind='slinear')
    px_min = min(px)
    px_max = max(px)
    if N==None:
        N = int(px_max)  # N is chosen as predicted by the particular model  ie, N = integral N(z) dz
    pxRandom = np.random.uniform(px_min,px_max,N)
    xrandom = px2x(pxRandom)
    return xrandom


def populate_constant_comoving (zmax=1,N=50000):
        z = gen_random (xmin=0,xmax=zmax,Px=dNz_dz_nonevolving,N=N)
        return z

def populate_evolving_gwtc2 (zmax=5,N=None):
    z = gen_random (xmin=0,xmax=zmax,Px=dNz_dz_gwtc2,N=N)
    return z


def uniform_location_orientation(N):
	# location choosen
	costheta = np.random.uniform(-1,1,N)
	theta    = np.arccos(costheta)
	phi      = np.random.uniform(0,2*np.pi,N)
	# orientation chosen
	psi      = np.random.uniform(0,2*np.pi,N)
	cosiota  = np.random.uniform(-1,1,N)
	iota	 = np.arccos(cosiota)
	return theta,phi,psi,iota


    
 
 

#########################    noise curves ###########################
# mention filetype 'asd' or 'psd'
# In some cases, nose curves are given as asd, in some cases as psd
# Do mention which one you have
def psdInterp(filename,filetype='psd'):
        data = np.loadtxt(filename)
        f   = data[:,0]
        snf = data[:,1]
        if filetype == 'asd':
                snf = snf**2
        snf_interp = si.interp1d(f, snf, kind='slinear')
        return snf_interp

# below are the interploted strings from files which can be directly used in fisher estimations
# the label on LHS is what is to be passed as qrgument in the fisher function
# if a new psd file (eg, ET, CE, LISA etc) is available, then add a line below for that with appropreate label
# so that it can be also used 
# CAUTION: whether the available file gives 'asd' or 'psd' must be CORRECT-ly mentioned.
aligo= psdInterp(filename='/home/muhammed.saleem/dakshasat/aLIGO_ZERO_DET_high_P_psd.txt',filetype='psd')


aligo_O4high= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/aligo_O4high.txt',filetype='asd')
avirgo_O4high_NEW= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/avirgo_O4high_NEW.txt',filetype='asd')

kagra_25Mpc= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/kagra_25Mpc.txt',filetype='asd')
kagra_80Mpc= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/kagra_80Mpc.txt',filetype='asd')

AplusDesign= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/AplusDesign.txt',filetype='asd')
avirgo_O5high_NEW= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/avirgo_O5high_NEW.txt',filetype='asd')
kagra_128Mpc= psdInterp(filename='/home/muhammed.saleem/dakshasat/observing-scenario-paper-2019-2020-update/kagra_128Mpc.txt',filetype='asd')


