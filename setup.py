# setup.py

from setuptools import setup, find_packages

setup(
    name='gwutils',
    version='0.1.0',
    packages=find_packages(),
    install_requires=[
        'numpy',
        'bilby',
        'pandas',
        'pycbc'
    ],
)

